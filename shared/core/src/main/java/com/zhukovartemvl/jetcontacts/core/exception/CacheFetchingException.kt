package com.zhukovartemvl.jetcontacts.core.exception


class CacheFetchingException(message: String = "Cache fetching exception"): Exception(message)
