package com.zhukovartemvl.jetcontacts.core.exception


class CallPermissionsDeniedException(message: String = "Call permissions denied exception"): Exception(message)
