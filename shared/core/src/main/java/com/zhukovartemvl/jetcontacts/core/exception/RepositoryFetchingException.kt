package com.zhukovartemvl.jetcontacts.core.exception


class RepositoryFetchingException(message: String = "Repository fetching exception"): Exception(message)
