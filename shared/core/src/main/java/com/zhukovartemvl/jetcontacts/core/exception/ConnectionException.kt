package com.zhukovartemvl.jetcontacts.core.exception


object ConnectionException: Exception("Check your internet connection!")
