package com.zhukovartemvl.jetcontacts.core.repository

import com.zhukovartemvl.jetcontacts.core.model.ContactDTO
import com.zhukovartemvl.jetcontacts.core.utils.Either


interface ContactRepository {
    suspend fun fetchContacts(force: Boolean = false): Either<Exception, List<ContactDTO>>
    suspend fun getContactByDbId(id: String): Either<Exception, ContactDTO>
}
