package com.zhukovartemvl.jetcontacts.core.model


data class ContactDTO(
    val id: String,
    val name: String,
    val phone: String,
    val height: Float,
    val biography: String,
    val temperament: Temperament,
    val educationPeriod: EducationPeriod
)

enum class Temperament { Melancholic, Phlegmatic, Sanguine, Choleric, Unknown }

data class EducationPeriod(val start: String, val end: String)
