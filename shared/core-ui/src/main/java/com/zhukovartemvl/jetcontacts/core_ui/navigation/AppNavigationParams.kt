package com.zhukovartemvl.jetcontacts.core_ui.navigation


object AppNavigationParams {

    object Screen {
        const val ContactsList = "contacts_list"
        fun ContactDetails(contactId: String) = Path.ContactDetails + contactId
    }

    object Destination {
        const val ContactDetails = Path.ContactDetails + "{" + Argument.ContactDbId + "}"
    }

    object Argument {
        const val ContactDbId = "contactDbId"
    }

    private object Path {
        const val ContactDetails = "contact_details/"
    }
}
