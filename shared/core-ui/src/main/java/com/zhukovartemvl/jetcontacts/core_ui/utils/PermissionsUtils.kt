package com.zhukovartemvl.jetcontacts.core_ui.utils

import android.app.Activity
import android.content.pm.PackageManager
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


@Composable
fun createPermissionLauncher(onGranted: () -> Unit, onDenied: () -> Unit) =
    rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
        if (isGranted)
            onGranted()
        else
            onDenied()
    }

@Composable
fun CheckPermissions(
    requestPermissionLauncher: ManagedActivityResultLauncher<String, Boolean>,
    permission: String,
    onGranted: () -> Unit
) {
    val context = LocalContext.current as Activity
    val permissionsGranted = PackageManager.PERMISSION_GRANTED
    when {
        ContextCompat.checkSelfPermission(context, permission) == permissionsGranted -> {
            onGranted()
        }
        ActivityCompat.shouldShowRequestPermissionRationale(context, permission) -> {
            requestPermissionLauncher.launch(permission)
        }
        else -> {
            requestPermissionLauncher.launch(permission)
        }
    }
}
