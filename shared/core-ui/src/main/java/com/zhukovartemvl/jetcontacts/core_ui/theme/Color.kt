package com.zhukovartemvl.jetcontacts.core_ui.theme

import androidx.compose.ui.graphics.Color


val ColorPrimary = Color(0xFF4c9069)
val ColorPrimaryDark = Color(0xFF3d7254)
val ColorAccent = Color(0xFF4d8f69)
val ColorBlackText = Color(0xe5000000)
val ColorGrayText = Color(0x8a000000)
val ColorClickableText = Color(0xFF269df7)
