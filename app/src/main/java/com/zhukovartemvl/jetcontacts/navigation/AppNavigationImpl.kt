package com.zhukovartemvl.jetcontacts.navigation

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.ComposeView
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import com.zhukovartemvl.jetcontacts.contacts.contact_details.ContactDetailsScreen
import com.zhukovartemvl.jetcontacts.contacts.contacts_list.ContactsListScreen
import com.zhukovartemvl.jetcontacts.core_ui.navigation.AppNavigationParams
import com.zhukovartemvl.jetcontacts.core_ui.navigation.AppNavigationView
import com.zhukovartemvl.jetcontacts.core_ui.theme.JetContactsTheme


class AppNavigationViewImpl : AppNavigationView {

    override fun content(context: Context) = ComposeView(context).apply {
        setContent { JetContactsTheme { NavigationHost() } }
    }

    @Composable
    private fun NavigationHost() {
        val navController = rememberNavController()

        NavHost(
            navController = navController,
            startDestination = AppNavigationParams.Screen.ContactsList
        ) {
            composable(AppNavigationParams.Screen.ContactsList) {
                ContactsListScreen(navController)
            }
            composable(AppNavigationParams.Destination.ContactDetails,
                arguments = listOf(
                    navArgument(AppNavigationParams.Argument.ContactDbId) { type = NavType.StringType }
                )
            ) { backStack ->
                val contactId = backStack.arguments?.getString(AppNavigationParams.Argument.ContactDbId) ?: ""
                ContactDetailsScreen(navController, contactId)
            }
        }
    }
}
