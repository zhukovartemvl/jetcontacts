package com.zhukovartemvl.jetcontacts.di

import com.zhukovartemvl.jetcontacts.data.di.dataModule


val moduleList = listOf(
    appModule,
    dataModule
)
