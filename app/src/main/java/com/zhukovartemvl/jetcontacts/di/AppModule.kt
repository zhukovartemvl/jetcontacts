package com.zhukovartemvl.jetcontacts.di

import com.zhukovartemvl.jetcontacts.core_ui.navigation.AppNavigationView
import com.zhukovartemvl.jetcontacts.navigation.AppNavigationViewImpl
import org.koin.dsl.module


val appModule = module {
    single { AppNavigationViewImpl() as AppNavigationView }
}
