dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "JetContacts"

include(":app")

include(":data")

include(":shared:core")
include(":shared:core-ui")
include(":shared:resources")

include(":feature:activity")
include(":feature:contacts")
