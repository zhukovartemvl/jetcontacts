package com.zhukovartemvl.jetcontacts.data.cache.impl.base

import com.zhukovartemvl.jetcontacts.core.exception.CacheFetchingException
import com.zhukovartemvl.jetcontacts.core.utils.Either
import com.zhukovartemvl.jetcontacts.core.utils.Failure
import com.zhukovartemvl.jetcontacts.core.utils.Success
import com.zhukovartemvl.jetcontacts.data.cache.base.Cache
import com.zhukovartemvl.jetcontacts.data.database.dao.base.CacheBaseDao
import com.zhukovartemvl.jetcontacts.data.preferences.CachePreferences


abstract class BaseCacheImpl<T>(private val preferences: CachePreferences) : Cache<T> {

    abstract val expirationTime: Long

    abstract val lastUpdateKey: String

    abstract val cacheDao: CacheBaseDao<T>

    override val isExpired: Boolean
        get() {
            val currentTime = System.currentTimeMillis()
            val lastUpdateTime = getLastCacheUpdateTimeMillis()
            val expired = currentTime - lastUpdateTime > expirationTime
            if (expired) {
                evictAll()
            }
            return expired
        }

    override fun getById(id: String): Either<Exception, T> {
        return try {
            val entity = cacheDao.getById(id)
            return Success(entity)
        } catch (e: Exception) {
            Failure(CacheFetchingException(e.toString()))
        }
    }

    override fun getAll(): Either<Exception, List<T>> {
        return try {
            val entity = cacheDao.getAll()
            return Success(entity)
        } catch (e: Exception) {
            Failure(CacheFetchingException(e.toString()))
        }
    }

    override fun putAll(entities: List<T>) {
        cacheDao.insertAll(entities)
        setLastCacheUpdateTimeMillis()
    }

    override fun replaceAll(entities: List<T>) {
        evictAll()
        putAll(entities)
    }

    override fun isCached(): Boolean {
        return cacheDao.itemsCount() != 0
    }

    override fun evictAll() {
        cacheDao.deleteAll()
    }

    private fun setLastCacheUpdateTimeMillis() {
        val currentMillis = System.currentTimeMillis()
        preferences.setLongValueByKey(lastUpdateKey, currentMillis)
    }

    private fun getLastCacheUpdateTimeMillis(): Long {
        return preferences.getLongValueByKey(lastUpdateKey)
    }

}
