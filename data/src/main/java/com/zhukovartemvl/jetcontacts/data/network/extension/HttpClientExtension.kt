package com.zhukovartemvl.jetcontacts.data.network.extension

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.zhukovartemvl.jetcontacts.core.exception.BadResponseException
import com.zhukovartemvl.jetcontacts.core.exception.NoInternetConnectionException
import com.zhukovartemvl.jetcontacts.core.utils.Failure
import com.zhukovartemvl.jetcontacts.core.utils.Success
import io.ktor.client.*
import kotlinx.serialization.SerializationException
import org.koin.core.context.GlobalContext.get as koin


val isNetworkAvailable: Boolean
    get() {
        val context: Context = koin().get()
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val n = cm.activeNetwork
            if (n != null) {
                val nc = cm.getNetworkCapabilities(n)
                //It will check for both wifi and cellular network
                return nc!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                    NetworkCapabilities.TRANSPORT_WIFI
                )
            }
            return false
        } else {
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }
    }

suspend fun <Value : Any> HttpClient.intercept(block: suspend () -> Value) =
    if (isNetworkAvailable) try {
        Success(block.invoke())
    } catch (e: Exception) {
        Failure(
            when (e) {
                is SerializationException -> BadResponseException
                else -> e
            }
        )
    } else Failure(NoInternetConnectionException)
