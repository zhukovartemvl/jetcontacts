package com.zhukovartemvl.jetcontacts.data.converter

import com.zhukovartemvl.jetcontacts.core.model.ContactDTO
import com.zhukovartemvl.jetcontacts.core.model.EducationPeriod
import com.zhukovartemvl.jetcontacts.core.model.Temperament
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity


fun ContactEntity.transform(): ContactDTO {
    return ContactDTO(
        id = id,
        name = name,
        phone = phone,
        height = height,
        biography = biography,
        temperament = temperament.getTemperament(),
        educationPeriod = EducationPeriod(educationPeriod.start, educationPeriod.end)
    )
}

private fun String.getTemperament() = when (this) {
    "melancholic" -> Temperament.Melancholic
    "phlegmatic" -> Temperament.Phlegmatic
    "sanguine" -> Temperament.Sanguine
    "choleric" -> Temperament.Choleric
    else -> Temperament.Unknown
}
