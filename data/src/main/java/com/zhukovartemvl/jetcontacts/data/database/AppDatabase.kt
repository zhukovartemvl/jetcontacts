package com.zhukovartemvl.jetcontacts.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.zhukovartemvl.jetcontacts.data.converter.EducationPeriodConverter
import com.zhukovartemvl.jetcontacts.data.database.dao.ContactCacheDao
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity


@Database(entities = [ContactEntity::class], version = 1, exportSchema = false)
@TypeConverters(EducationPeriodConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun contactCacheDao(): ContactCacheDao

}
