package com.zhukovartemvl.jetcontacts.data.cache.impl

import com.zhukovartemvl.jetcontacts.data.cache.ContactCache
import com.zhukovartemvl.jetcontacts.data.cache.impl.base.BaseCacheImpl
import com.zhukovartemvl.jetcontacts.data.database.AppDatabase
import com.zhukovartemvl.jetcontacts.data.database.dao.base.CacheBaseDao
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity
import com.zhukovartemvl.jetcontacts.data.preferences.CachePreferences


class ContactCacheImpl(preferences: CachePreferences, database: AppDatabase) :
    BaseCacheImpl<ContactEntity>(preferences), ContactCache {

    override val expirationTime: Long = 60000 //1 minute
    override val lastUpdateKey: String = "ContactLastUpdateKey"
    override val cacheDao: CacheBaseDao<ContactEntity> = database.contactCacheDao()

}
