package com.zhukovartemvl.jetcontacts.data.cache.base

import com.zhukovartemvl.jetcontacts.core.utils.Either


interface Cache<T> {
    val isExpired: Boolean

    @Throws(Exception::class)
    fun getById(id: String): Either<Exception, T>

    @Throws(Exception::class)
    fun getAll(): Either<Exception, List<T>>

    @Throws(Exception::class)
    fun putAll(entities: List<T>)

    @Throws(Exception::class)
    fun replaceAll(entities: List<T>)

    @Throws(Exception::class)
    fun isCached(): Boolean

    @Throws(Exception::class)
    fun evictAll()
}
