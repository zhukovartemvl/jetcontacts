package com.zhukovartemvl.jetcontacts.data.database.dao

import androidx.room.*
import androidx.room.OnConflictStrategy
import com.zhukovartemvl.jetcontacts.data.database.dao.base.CacheBaseDao
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity


@Dao
interface ContactCacheDao : CacheBaseDao<ContactEntity> {

    @Query("Select * from contact where id = :id limit 1")
    override fun getById(id: String): ContactEntity

    @Query("Select * from contact")
    override fun getAll(): List<ContactEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun insertAll(entities: List<ContactEntity>)

    @Query("Delete from contact")
    override fun deleteAll()

    @Query("Select count(*) from contact")
    override fun itemsCount(): Int

}
