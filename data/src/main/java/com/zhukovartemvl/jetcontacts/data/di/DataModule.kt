package com.zhukovartemvl.jetcontacts.data.di

import android.content.Context
import androidx.room.Room
import com.zhukovartemvl.jetcontacts.core.repository.ContactRepository
import com.zhukovartemvl.jetcontacts.data.R
import com.zhukovartemvl.jetcontacts.data.cache.ContactCache
import com.zhukovartemvl.jetcontacts.data.cache.impl.ContactCacheImpl
import com.zhukovartemvl.jetcontacts.data.database.AppDatabase
import com.zhukovartemvl.jetcontacts.data.network.api.ContactApi
import com.zhukovartemvl.jetcontacts.data.network.api.impl.ContactApiImpl
import com.zhukovartemvl.jetcontacts.data.preferences.CachePreferences
import com.zhukovartemvl.jetcontacts.data.preferences.impl.CachePreferencesImpl
import com.zhukovartemvl.jetcontacts.data.repository.ContactRepositoryImpl
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import kotlinx.serialization.json.Json
import org.koin.dsl.module


val dataModule = module {
    single { getDatabase(get()) }
    factory { ContactRepositoryImpl(get(), get()) as ContactRepository }

    single { createJson() }
    single { createHttpClient(get()) }
    factory { ContactApiImpl(get()) as ContactApi }

    factory { CachePreferencesImpl(get()) as CachePreferences }

    factory { ContactCacheImpl(get(), get()) as ContactCache }
}

private fun getDatabase(context: Context) =
    Room.databaseBuilder(context, AppDatabase::class.java, context.getString(R.string.db_name))
        .fallbackToDestructiveMigration()
        .build()

private fun createJson() = Json { isLenient = true; ignoreUnknownKeys = true }

private fun createHttpClient(json: Json) = HttpClient(Android) {
    install(JsonFeature) {
        serializer = KotlinxSerializer(json)
    }
    engine {
        connectTimeout = 10_000
    }
}
