package com.zhukovartemvl.jetcontacts.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
@Entity(tableName = "contact")
data class ContactEntity(
    @PrimaryKey @SerialName("id") val id: String,
    @SerialName("name") val name: String,
    @SerialName("phone") val phone: String,
    @SerialName("height") val height: Float,
    @SerialName("biography") val biography: String,
    @SerialName("temperament") val temperament: String,
    @SerialName("educationPeriod") val educationPeriod: EducationPeriod
)

@Serializable
data class EducationPeriod(
    @SerialName("start") val start: String,
    @SerialName("end") val end: String
)
