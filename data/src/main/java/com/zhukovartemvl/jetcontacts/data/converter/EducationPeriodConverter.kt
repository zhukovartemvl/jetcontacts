package com.zhukovartemvl.jetcontacts.data.converter

import androidx.room.TypeConverter
import com.zhukovartemvl.jetcontacts.data.entity.EducationPeriod
import kotlinx.serialization.json.Json


object EducationPeriodConverter {
    @TypeConverter
    fun fromString(value: String): EducationPeriod {
        return Json.decodeFromString(EducationPeriod.serializer(), value)
    }

    @TypeConverter
    fun fromEducationPeriod(educationPeriod: EducationPeriod): String {
        return Json.encodeToString(EducationPeriod.serializer(), educationPeriod)
    }
}
