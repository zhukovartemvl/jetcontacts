package com.zhukovartemvl.jetcontacts.data.network.api

import com.zhukovartemvl.jetcontacts.core.utils.Either
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity


interface ContactApi {
    suspend fun fetchContacts(url: String): Either<Exception, List<ContactEntity>>
}
