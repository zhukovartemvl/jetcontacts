package com.zhukovartemvl.jetcontacts.data.network.api.impl

import com.zhukovartemvl.jetcontacts.core.utils.Either
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity
import com.zhukovartemvl.jetcontacts.data.network.api.ContactApi
import com.zhukovartemvl.jetcontacts.data.network.extension.intercept
import io.ktor.client.*
import io.ktor.client.request.*
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json


class ContactApiImpl(private val client: HttpClient) : ContactApi {

    override suspend fun fetchContacts(url: String): Either<Exception, List<ContactEntity>> {
        return client.intercept {
            val response = client.get<String>(urlString = url)
            Json.decodeFromString(ListSerializer(ContactEntity.serializer()), response)
        }
    }
}
