package com.zhukovartemvl.jetcontacts.data.cache

import com.zhukovartemvl.jetcontacts.data.cache.base.Cache
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity


interface ContactCache : Cache<ContactEntity>
