package com.zhukovartemvl.jetcontacts.data.database.dao.base


interface CacheBaseDao<T> {

    fun getById(id: String): @JvmSuppressWildcards T

    fun getAll(): List<@JvmSuppressWildcards T>

    fun insertAll(entities: List<@JvmSuppressWildcards T>)

    fun deleteAll()

    fun itemsCount(): Int

}
