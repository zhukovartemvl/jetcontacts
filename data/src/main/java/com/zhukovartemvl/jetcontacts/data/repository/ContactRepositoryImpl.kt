package com.zhukovartemvl.jetcontacts.data.repository

import com.zhukovartemvl.jetcontacts.core.exception.RepositoryFetchingException
import com.zhukovartemvl.jetcontacts.core.model.ContactDTO
import com.zhukovartemvl.jetcontacts.core.repository.ContactRepository
import com.zhukovartemvl.jetcontacts.core.utils.*
import com.zhukovartemvl.jetcontacts.data.cache.ContactCache
import com.zhukovartemvl.jetcontacts.data.converter.transform
import com.zhukovartemvl.jetcontacts.data.entity.ContactEntity
import com.zhukovartemvl.jetcontacts.data.network.api.ContactApi
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope


class ContactRepositoryImpl(
    private val contactCache: ContactCache,
    private val contactApi: ContactApi
) : ContactRepository {

    private val dataSourcesUrls = listOf(
        "https://raw.githubusercontent.com/SkbkonturMobile/mobile-test-droid/master/json/generated-01.json",
        "https://raw.githubusercontent.com/SkbkonturMobile/mobile-test-droid/master/json/generated-02.json",
        "https://raw.githubusercontent.com/SkbkonturMobile/mobile-test-droid/master/json/generated-03.json"
    )

    override suspend fun fetchContacts(force: Boolean): Either<Exception, List<ContactDTO>> {
        return if (force || !contactCache.isCached() || contactCache.isExpired)
            fetchContactsFromNetwork()
        else
            fetchContactsFromCache()
    }

    override suspend fun getContactByDbId(id: String): Either<Exception, ContactDTO> {
        contactCache.getById(id)
            .doOnSuccess { contact ->
                return Success(contact.transform())
            }
            .doOnFailure { exception ->
                return Failure(exception)
            }
        return Failure(RepositoryFetchingException())
    }

    private suspend fun fetchContactsFromNetwork(): Either<Exception, List<ContactDTO>> {
        val contactsList = arrayListOf<ContactEntity>()
        var exception: Exception? = null

        //Parallel requests
        coroutineScope {
            val responses = arrayListOf<Deferred<Either<Exception, List<ContactEntity>>>>()

            dataSourcesUrls.forEach { dataSourcesUrl ->
                responses.add(async { contactApi.fetchContacts(dataSourcesUrl) })
            }

            responses.forEach { response ->
                response.await()
                    .doOnSuccess { contacts -> contactsList.addAll(contacts) }
                    .doOnFailure { exception = it }
            }
        }

        return if (contactsList.isEmpty()) {
            exception?.let { Failure(it) } ?: fetchContactsFromCache()
        } else {
            contactCache.replaceAll(contactsList)
            val contactDTOList = contactsList.map { it.transform() }
            Success(contactDTOList)
        }
    }

    private fun fetchContactsFromCache(): Either<Exception, List<ContactDTO>> {
        contactCache.getAll().doOnSuccess { contacts ->
            val coinDTOList = contacts.map { it.transform() }
            return Success(coinDTOList)
        }.doOnFailure { exception ->
            return Failure(exception)
        }
        return Failure(RepositoryFetchingException())
    }

}
