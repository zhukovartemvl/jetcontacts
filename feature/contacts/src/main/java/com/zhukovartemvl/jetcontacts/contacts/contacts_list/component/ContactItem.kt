package com.zhukovartemvl.jetcontacts.contacts.contacts_list.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
internal fun ContactItem(
    name: String,
    phone: String,
    height: Float,
    onClick: () -> Unit
) {
    Card(elevation = 4.dp) {
        Column(
            modifier = Modifier
                .clickable(onClick = onClick)
                .height(70.dp)
                .padding(horizontal = 16.dp, vertical = 10.dp)
        ) {
            Row {
                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(bottom = 4.dp),
                    text = name,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.body1
                )
                Text(
                    text = height.toString(),
                    style = MaterialTheme.typography.subtitle1
                )
            }
            Text(
                text = phone,
                style = MaterialTheme.typography.subtitle1
            )
        }
    }
}

@Preview
@Composable
private fun ContactItemPreview() {
    ContactItem(
        name = "Summer Greer",
        phone = "+7 (903) 425-3032",
        height = 201.9f,
        onClick = {}
    )
}
