package com.zhukovartemvl.jetcontacts.contacts.contact_details

import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.UiAction
import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.UiEvent
import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.UiState


class ContactDetailsScreenContract {

    data class State(
        val name: String = "",
        val phone: String = "",
        val temperament: String = "",
        val educationPeriod: String = "",
        val biography: String = "",
        val screenState: ScreenState = ScreenState.Loading
    ) : UiState

    sealed class ScreenState {
        object Loading : ScreenState()
        object Default : ScreenState()
    }

    sealed class Event : UiEvent {
        object OnPhoneNumberClicked : Event()
        object OnCallPermissionsGranted : Event()
        object OnCallPermissionsDenied : Event()
    }

    sealed class Action : UiAction {
        object None : Action()
        object CheckCallPermissions : Action()
        data class MakeCall(val phoneNumber: String) : Action()
        data class Error(val error: Exception) : Action()
    }

}
