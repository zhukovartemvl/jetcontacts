package com.zhukovartemvl.jetcontacts.contacts.contacts_list

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.zhukovartemvl.jetcontacts.contacts.contacts_list.ContactsListScreenContract.*
import com.zhukovartemvl.jetcontacts.contacts.contacts_list.component.ContactItem
import com.zhukovartemvl.jetcontacts.contacts.contacts_list.component.ContactShimmers
import com.zhukovartemvl.jetcontacts.contacts.contacts_list.component.SearchAppBar
import com.zhukovartemvl.jetcontacts.core.model.ContactDTO
import com.zhukovartemvl.jetcontacts.core_ui.navigation.AppNavigationParams
import kotlinx.coroutines.launch


@Composable
fun ContactsListScreen(
    navController: NavController,
    viewModel: ContactsListScreenViewModel = viewModel()
) {
    val state by viewModel.uiState.collectAsState()
    val action by viewModel.action.collectAsState(initial = Action.None)

    val snackbarCoroutineScope = rememberCoroutineScope()
    val scaffoldState = rememberScaffoldState()

    when (val collectedAction = action) {
        is Action.Error -> {
            collectedAction.error.localizedMessage?.let { message ->
                snackbarCoroutineScope.launch {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message,
                        duration = SnackbarDuration.Short
                    )
                }
            }
        }
        Action.None -> Unit
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            val focusManager = LocalFocusManager.current
            SearchAppBar(
                searchFilter = state.searchFilter,
                onTextChanged = { newSearchFilter ->
                    if (newSearchFilter == "") {
                        focusManager.clearFocus()
                    }
                    viewModel.setEvent(Event.OnSearchFilterChanged(newSearchFilter))
                },
                onClear = {
                    focusManager.clearFocus()
                    viewModel.setEvent(Event.OnSearchFilterChanged(""))
                }
            )
        }
    ) {
        val swipeRefreshState =
            rememberSwipeRefreshState(state.screenState == ScreenState.Refreshing)
        SwipeRefresh(
            state = swipeRefreshState,
            onRefresh = { viewModel.setEvent(Event.Refresh) }
        ) {
            when (val screenState = state.screenState) {
                ScreenState.Loading -> ContactShimmers()
                ScreenState.Refreshing -> ContactShimmers()
                ScreenState.Default -> {
                    ContactsList(
                        contacts = state.contacts,
                        onClick = { contactId ->
                            navController.navigate(
                                AppNavigationParams.Screen.ContactDetails(
                                    contactId
                                )
                            )
                        }
                    )
                }
                is ScreenState.Error -> {
                    Text(text = screenState.message)
                }
            }
        }
    }
}

@Composable
private fun ContactsList(contacts: List<ContactDTO>, onClick: (id: String) -> Unit) {
    LazyColumn {
        items(contacts) { contact ->
            ContactItem(
                name = contact.name,
                phone = contact.phone,
                height = contact.height,
                onClick = {
                    onClick(contact.id)
                }
            )
        }
        item {
            Spacer(modifier = Modifier.height(60.dp))
        }
    }
}

//@Composable
//private fun Snackbar(snackbarHostState: SnackbarHostState, action: Action) {
//
//}
