package com.zhukovartemvl.jetcontacts.contacts.contact_details

import android.Manifest
import android.app.Activity
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.zhukovartemvl.jetcontacts.contacts.contact_details.ContactDetailsScreenContract.*
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.filled.Call
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import com.zhukovartemvl.jetcontacts.core_ui.theme.ColorClickableText
import com.zhukovartemvl.jetcontacts.core_ui.utils.CheckPermissions
import com.zhukovartemvl.jetcontacts.core_ui.utils.createPermissionLauncher


@Composable
fun ContactDetailsScreen(
    navController: NavController,
    contactId: String,
    viewModel: ContactDetailsScreenViewModel = viewModel()
) {
    val state by viewModel.uiState.collectAsState()
    val action by viewModel.action.collectAsState(initial = Action.None)

    viewModel.init(contactId)

    val context = LocalContext.current as Activity

    val requestPermissionLauncher = createPermissionLauncher(
        onGranted = { viewModel.setEvent(Event.OnCallPermissionsGranted) },
        onDenied = { viewModel.setEvent(Event.OnCallPermissionsDenied) }
    )

    when (val collectedAction = action) {
        Action.CheckCallPermissions -> {
            CheckPermissions(
                requestPermissionLauncher = requestPermissionLauncher,
                permission = Manifest.permission.CALL_PHONE,
                onGranted = { viewModel.setEvent(Event.OnCallPermissionsGranted) }
            )
        }
        is Action.MakeCall -> {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:" + collectedAction.phoneNumber)
            context.startActivity(callIntent)
        }
        is Action.Error -> {
            collectedAction.error.localizedMessage?.let { message ->
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        }
        Action.None -> Unit
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            tint = MaterialTheme.colors.surface,
                            contentDescription = null
                        )
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(20.dp)
        ) {
            Text(
                text = state.name,
                style = MaterialTheme.typography.h5
            )
            Spacer(modifier = Modifier.height(5.dp))
            TextButton(
                colors = ButtonDefaults.buttonColors(
                    contentColor = ColorClickableText,
                    backgroundColor = Color.Transparent
                ),
                onClick = { viewModel.setEvent(Event.OnPhoneNumberClicked) }
            ) {
                Icon(
                    modifier = Modifier.size(20.dp),
                    imageVector = Icons.Default.Call, contentDescription = null
                )
                Spacer(modifier = Modifier.width(10.dp))
                Text(text = state.phone)
            }
            Spacer(modifier = Modifier.height(5.dp))
            Text(
                text = state.temperament,
                style = MaterialTheme.typography.subtitle2
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = state.educationPeriod,
                style = MaterialTheme.typography.subtitle2
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = state.biography,
                style = MaterialTheme.typography.body2
            )
        }
    }
}
