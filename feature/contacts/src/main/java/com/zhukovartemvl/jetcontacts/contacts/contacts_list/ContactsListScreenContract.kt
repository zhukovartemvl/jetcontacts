package com.zhukovartemvl.jetcontacts.contacts.contacts_list

import com.zhukovartemvl.jetcontacts.core.model.ContactDTO
import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.UiAction
import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.UiEvent
import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.UiState


class ContactsListScreenContract {

    data class State(
        val contacts: List<ContactDTO> = listOf(),
        val searchFilter: String = "",
        val screenState: ScreenState = ScreenState.Loading
    ) : UiState

    sealed class ScreenState {
        object Loading : ScreenState()
        object Default : ScreenState()
        object Refreshing : ScreenState()
        data class Error(val message: String) : ScreenState()
    }

    sealed class Event : UiEvent {
        data class OnSearchFilterChanged(val newSearchFilter: String) : Event()
        object Refresh : Event()
    }

    sealed class Action : UiAction {
        object None : Action()
        data class Error(val error: Exception) : Action()
    }

}
