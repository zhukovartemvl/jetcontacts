package com.zhukovartemvl.jetcontacts.contacts.contacts_list.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import com.zhukovartemvl.jetcontacts.resources.R


@Composable
internal fun SearchAppBar(
    searchFilter: String,
    onTextChanged: (text: String) -> Unit,
    onClear: () -> Unit
) {
    TopAppBar(modifier = Modifier
        .height(70.dp)
        .background(color = Color.Transparent)
    ) {
        TextField(
            value = searchFilter,
            textStyle = MaterialTheme.typography.subtitle1,
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp),
            onValueChange = { onTextChanged(it) },
            leadingIcon = {
                Icon(
                    modifier = Modifier.size(20.dp),
                    imageVector = Icons.Default.Search,
                    contentDescription = null
                )
            },
            trailingIcon = {
                if (searchFilter.isNotEmpty()) {
                    IconButton(
                        modifier = Modifier.size(24.dp),
                        onClick = onClear
                    ) {
                        Icon(
                            modifier = Modifier.size(20.dp),
                            imageVector = Icons.Default.Close,
                            contentDescription = null
                        )
                    }
                }
            },
            placeholder = {
                Text(
                    text = stringResource(id = R.string.search),
                    style = MaterialTheme.typography.subtitle2
                )
            },
            shape = RoundedCornerShape(4.dp),
            colors = TextFieldDefaults.textFieldColors(
                textColor = MaterialTheme.colors.onPrimary,
                backgroundColor = MaterialTheme.colors.background,
                placeholderColor = MaterialTheme.colors.onSecondary,
                leadingIconColor = MaterialTheme.colors.onSecondary,
                trailingIconColor = MaterialTheme.colors.onSecondary,
                cursorColor = Color.Transparent,
            )
        )
    }
}

@Preview
@Composable
private fun SearchAppBarPreview() {
    var searchFilter by remember { mutableStateOf("") }
    SearchAppBar(
        searchFilter = searchFilter,
        onTextChanged = { newSearchFilter -> searchFilter = newSearchFilter },
        onClear = { searchFilter = "" }
    )
}
