package com.zhukovartemvl.jetcontacts.contacts.contacts_list

import androidx.lifecycle.viewModelScope
import com.zhukovartemvl.jetcontacts.core.repository.ContactRepository
import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.BaseViewModel
import com.zhukovartemvl.jetcontacts.contacts.contacts_list.ContactsListScreenContract.*
import com.zhukovartemvl.jetcontacts.core.model.ContactDTO
import com.zhukovartemvl.jetcontacts.core.utils.doOnFailure
import com.zhukovartemvl.jetcontacts.core.utils.doOnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


class ContactsListScreenViewModel : BaseViewModel<Event, State, Action>(), KoinComponent {

    private val contactRepository: ContactRepository by inject()

    private var contactsFullList: List<ContactDTO> = listOf()

    init {
        if (currentState.screenState == ScreenState.Loading) {
            loadContacts()
        }
    }

    override fun createInitialState() = State()

    override fun handleEvent(event: Event) {
        when (event) {
            is Event.OnSearchFilterChanged -> {
                setState { copy(searchFilter = event.newSearchFilter) }
                showContacts()
            }
            Event.Refresh -> {
                setState { copy(screenState = ScreenState.Refreshing) }
                loadContacts(true)
            }
        }
    }

    private fun loadContacts(force: Boolean = false) = viewModelScope.launch(Dispatchers.IO) {
        contactRepository.fetchContacts(force)
            .doOnSuccess { contacts ->
                contactsFullList = contacts
                showContacts()
            }.doOnFailure { exception ->
                setAction { Action.None }
                setAction { Action.Error(exception) }
                setState { copy(screenState = ScreenState.Default) }
            }
    }

    private fun showContacts() {
        val filter = currentState.searchFilter
        val filteredContacts = contactsFullList.filter {
            it.name.contains(filter, ignoreCase = true) || it.phone.contains(
                filter,
                ignoreCase = true
            )
        }
        setState { copy(contacts = filteredContacts, screenState = ScreenState.Default) }
    }
}
