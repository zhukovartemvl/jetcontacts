package com.zhukovartemvl.jetcontacts.contacts.contact_details

import androidx.lifecycle.viewModelScope
import com.zhukovartemvl.jetcontacts.core_ui.mvi_base.BaseViewModel
import com.zhukovartemvl.jetcontacts.contacts.contact_details.ContactDetailsScreenContract.*
import com.zhukovartemvl.jetcontacts.core.exception.CallPermissionsDeniedException
import com.zhukovartemvl.jetcontacts.core.model.ContactDTO
import com.zhukovartemvl.jetcontacts.core.repository.ContactRepository
import com.zhukovartemvl.jetcontacts.core.utils.doOnFailure
import com.zhukovartemvl.jetcontacts.core.utils.doOnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


class ContactDetailsScreenViewModel : BaseViewModel<Event, State, Action>(), KoinComponent {

    private val contactRepository: ContactRepository by inject()

    fun init(contactId: String) = viewModelScope.launch(Dispatchers.IO) {
        if (currentState.screenState == ScreenState.Loading) {
            contactRepository.getContactByDbId(contactId)
                .doOnSuccess { contact ->
                    showContact(contact)
                }
                .doOnFailure { exception ->
                    setAction { Action.Error(exception) }
                }
        }
    }

    private fun showContact(contact: ContactDTO) {

        val educationPeriodStart = contact.educationPeriod.start.dateFormat()
        val educationPeriodEnd = contact.educationPeriod.end.dateFormat()

        setState {
            copy(
                name = contact.name,
                phone = contact.phone,
                temperament = contact.temperament.toString(),
                educationPeriod = "$educationPeriodStart - $educationPeriodEnd",
                biography = contact.biography,
                screenState = ScreenState.Default
            )
        }
    }

    private fun String.dateFormat(): String {
        return this.replace("""(T.*)""".toRegex(), "").replace('-', '.')
    }

    override fun createInitialState() = State()

    override fun handleEvent(event: Event) {
        when (event) {
            Event.OnPhoneNumberClicked -> {
                setAction { Action.CheckCallPermissions }
            }
            Event.OnCallPermissionsGranted -> {
                setAction { Action.MakeCall(phoneNumber = currentState.phone) }
            }
            Event.OnCallPermissionsDenied -> {
                setAction { Action.Error(CallPermissionsDeniedException()) }
            }
        }
    }

}
